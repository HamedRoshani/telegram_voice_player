import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:test_opus/wave_slider.dart';

class Test extends StatefulWidget {
  const Test({super.key});

  @override
  State<Test> createState() => _TestState();
}

class _TestState extends State<Test> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Center(
                child: Container(
                  alignment: Alignment.topRight,
                  child: FittedBox(child: AudioWaveSlider(assetFile: "assets/soundTest155opus.opus")),
                  decoration: BoxDecoration(color: Colors.green),
                ),
              ),
            ],
          ),
        ],
      ),
    ));
  }
}
