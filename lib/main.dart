import 'package:flutter/material.dart';
import 'package:test_opus/test_widget.dart';
import 'package:test_opus/wave_slider.dart';
// import 'package:just_audio/just_audio.dart';
import 'wave_slider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        sliderTheme: SliderThemeData(
          trackShape: CustomTrackShape(),
          thumbShape: SliderComponentShape.noOverlay,
          thumbColor: Colors.transparent,
          minThumbSeparation: 0,
          overlayColor: Colors.transparent,
          activeTrackColor: Colors.transparent,
          disabledThumbColor: Colors.transparent,
          inactiveTrackColor: Colors.transparent,
          activeTickMarkColor: Colors.transparent,
          valueIndicatorColor: Colors.transparent,
          inactiveTickMarkColor: Colors.transparent,
          disabledActiveTrackColor: Colors.transparent,
          disabledInactiveTrackColor: Colors.transparent,
          disabledActiveTickMarkColor: Colors.transparent,
          overlappingShapeStrokeColor: Colors.transparent,
          disabledInactiveTickMarkColor: Colors.transparent,
        ),
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const Test(),
    );
  }
}

class VoicePlayer extends StatefulWidget {
  const VoicePlayer({super.key});

  @override
  State<VoicePlayer> createState() => _VoicePlayerState();
}

class _VoicePlayerState extends State<VoicePlayer> {
  // final AudioPlayer player1 = AudioPlayer();
  late Duration? musicDuration;

  @override
  void initState() {
    // player1.setAsset("assets/testVoice.opus").then((value) {
    //   setState(() {
    //     musicDuration = value;
    //   });
    // });

    super.initState();
  }

  @override
  void dispose() {
    // player1.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          alignment: Alignment.center,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              GestureDetector(
                // onTap: () {
                //   player1.playing ? player1.pause() : player1.play();
                // },
                child: Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    shape: BoxShape.circle,
                  ),
                  // child: player1.playing ? Icon(Icons.pause) : Icon(Icons.play_arrow),
                ),
              ),
              Slider(
                max: 1.0,
                min: 0.0,
                value: 0,
                onChanged: (value) {},
              )
            ],
          ),
        ),
      ),
    );
  }
}
