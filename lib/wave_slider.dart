import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_audio_waveforms/flutter_audio_waveforms.dart';
import 'package:just_audio/just_audio.dart';
import 'package:just_waveform/just_waveform.dart';
import 'package:path_provider/path_provider.dart';

class AudioWaveSlider extends StatefulWidget {
  const AudioWaveSlider({super.key, this.voiceFile, this.assetFile, this.urlFile});
  final File? voiceFile;
  final String? assetFile;
  final String? urlFile;

  @override
  State<AudioWaveSlider> createState() => _AudioWaveSliderState();
}

class _AudioWaveSliderState extends State<AudioWaveSlider> {
  // late PlayerController playerController;
  bool playing = false;
  final player = AudioPlayer();
  late Duration musicduration;
  late Duration audioPosition = Duration();
  List<double> randomnum = [];
  bool isloaded = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    final List<double> doubls = List<double>.generate(80, (index) => Random().nextDouble());
    randomnum = doubls;

    widget.voiceFile != null
        ? player.setFilePath(widget.voiceFile!.path).then((value) {
            setState(() {
              // musicduration = value!;
              isloaded = true;
            });
          })
        : widget.assetFile != null
            ? player.setAsset(widget.assetFile!).then((value) {
                setState(() {
                  // musicduration = value!;
                  isloaded = true;
                });
              })
            : player.setUrl(widget.urlFile!).then((value) {
                setState(() {
                  // musicduration = value!;
                  isloaded = true;
                });
              });

    player.positionStream.listen((event) {
      setState(() {
        audioPosition = Duration(seconds: event.inSeconds);
        if (audioPosition == musicduration) {
          player.load();
          player.pause();
        }
      });
    });

    player.durationStream.listen((event) {
      setState(() {
        musicduration = Duration(seconds: event!.inSeconds!);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(maxWidth: 150.w() * .7),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          GestureDetector(
            onTap: () {
              if (player.playing) {
                player.pause();
              } else if (!player.playing) {
                player.play();
              }
            },
            child: Container(
              margin: EdgeInsets.only(top: 10, bottom: 10, left: 10),
              width: 40,
              height: 40,
              child: Icon(
                player.playing ? Icons.pause : Icons.play_arrow,
                size: 25,
              ),
              decoration: BoxDecoration(color: Colors.white, shape: BoxShape.circle),
            ),
          ),
          isloaded
              ? Padding(
                  padding: EdgeInsets.only(top: 10, left: 10, right: 10),
                  child: Stack(
                    clipBehavior: Clip.none,
                    alignment: Alignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 5),
                        child: RectangleWaveform(
                          samples: randomnum,
                          height: 12.5.w(),

                          width: musicduration.inSeconds >= 50 ? 50.5.w() : 30.w(),
                          maxDuration: musicduration,
                          // absolute: true,
                          elapsedDuration: audioPosition,
                          // isCentered: true,
                          // style: PaintingStyle.fill,
                          // absolute: true,

                          // absolute: true,
                          invert: true,
                          showActiveWaveform: true,
                        ),
                      ),
                      SizedBox(
                        width: musicduration.inSeconds >= 50 ? 50.5.w() : 30.w(),
                        child: Slider(
                          max: musicduration!.inSeconds.toDouble(),
                          min: 0.0,
                          value: audioPosition!.inSeconds.toDouble(),
                          onChanged: (value) {
                            final pusitionChanged = Duration(seconds: value.toInt());
                            player.seek(pusitionChanged);
                          },
                        ),
                      ),
                    ],
                  ),
                )
              : SizedBox(
                  width: 30,
                  height: 30,
                  child: CircularProgressIndicator(
                    color: Colors.white,
                  )),
          GestureDetector(
            onTap: () {
              player.speed == 1
                  ? player.setSpeed(1.5)
                  : player.speed == 1.5
                      ? player.setSpeed(2)
                      : player.setSpeed(1);
            },
            child: Container(
              margin: EdgeInsets.only(right: 10),
              width: 40,
              height: 20,
              decoration: BoxDecoration(
                color: Colors.grey,
                borderRadius: BorderRadius.circular(15),
              ),
              child: Center(
                child: Text(
                  player.speed == 1
                      ? "1x"
                      : player.speed == 1.5
                          ? "1.5x"
                          : "2x",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class CustomTrackShape extends RoundedRectSliderTrackShape {
  /// document will be added
  @override
  Rect getPreferredRect({
    required RenderBox parentBox,
    Offset offset = Offset.zero,
    required SliderThemeData sliderTheme,
    bool isEnabled = false,
    bool isDiscrete = false,
  }) {
    const double trackHeight = 5;
    final double trackLeft = offset.dx;
    final double trackTop = offset.dy + (parentBox.size.height - trackHeight) / 2;
    final double trackWidth = parentBox.size.width;
    return Rect.fromLTWH(trackLeft, trackTop, trackWidth, trackHeight);
  }
}

final MediaQueryData media = MediaQueryData.fromWindow(WidgetsBinding.instance!.window);

/// This extention help us to make widget responsive.
extension NumberParsing on num {
  double w() => this * media.size.width / 100;

  double h() => this * media.size.height / 100;
}
